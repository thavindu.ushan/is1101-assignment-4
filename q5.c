#include <stdio.h>

int main()
{
    int A = 10, B = 15;
    int and, xor, not, lShift, rShift;

    and = (A&B);
    xor = (A^B);
    not = (~A);
    lShift = (A<<3);
    rShift = (B>>3);

    printf("A = %d   B = %d\n\n", A,B);
    printf("A&B  : %d\n", and);
    printf("A^B  : %d\n", xor);
    printf("~A   : %d\n", not);
    printf("A<<3 : %d\n", lShift);
    printf("B>>3 : %d\n", rShift);

    return 0;

}
