#include <stdio.h>

int main()
{
    int num1, num2;

    printf("First number  : ");
    scanf("%d", &num1);
    printf("Second number : ");
    scanf("%d", &num2);
    printf("\n");

    num1 = num1 + num2;
    num2 = num1 - num2;
    num1 = num1 - num2;

    printf("After swapping;\n\n");
    printf("First number  : %d\n", num1);
    printf("Second number : %d\n", num2);

    return 0;

}
