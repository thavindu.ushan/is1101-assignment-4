#include <stdio.h>

int main()
{
    float r, h, vol;

    printf("Enter the radius of the cone : ");
    scanf("%f", &r);
    printf("Enter the height of the cone : ");
    scanf("%f", &h);
    printf("\n");

    const float pi = 3.142;
    vol = pi * (r*r) * (h/3);
    printf("Volume of the cone is        : %f\n", vol);

    return 0;
}
