#include <stdio.h>

int main()
{
    int num1, num2, num3, total, avg;

    printf("Enter the first number : ");
    scanf("%d", &num1);
    printf("Enter the second number: ");
    scanf("%d", &num2);
    printf("Enter the third number : ");
    scanf("%d", &num3);
    printf("\n");

    total = num1 + num2 + num3;
    printf("Total   = %d\n", total);

    avg = total / 3;
    printf("Average = %d\n", avg);

    return 0;

}
